# Form builder
### Description:
Application enables to create structure of conditional forms. One can choose a anwser to which components will be shown. There is also possibility to select input type.

#### Technologies used:
* react,
* redux,
* javascript,
* sass,
* html

### How to run?
1. _npm_ _install_
2. _npm_ _run_ _start_

<img src="https://drive.google.com/uc?export=view&id=16nQqcH5PVZo5XRzzImdqdVZdrxfFxc3n" width="500" height="auto">