import React from 'react';
import ReactDOM from 'react-dom';

import App from './js/components/App';
import './sass/main.sass'

ReactDOM.render(<App />, document.getElementById('root'));

