import uuid from 'uuid';
import store from '../store';
import {ADD_FORM, GET_FORMS, DELETE_FORM, UPDATE_FORM, REFRESH_STATE} from './types'

import {connectDb, get} from '../helpers'

export const addForm = (parent = store.getState().forms.forms) => dispatch => {

  const draft = {
    parent,
    question: '',
    type: 'text',
    answer: '',
    condition: 'equals',
    children: [],
    id: uuid()
  };

  dispatch({
    type: ADD_FORM,
    payload: {
      draft
    }
  });
};

export const getForms = () => {
  return {
    type: GET_FORMS,
    payload: {}
  };
};

export const deleteForm = (form) => dispatch => {

  dispatch({
    type: DELETE_FORM,
    payload: form
  });

};
//
export const updateForm = (form, id, type) => dispatch => {
  dispatch({
    type: UPDATE_FORM,
    payload: {
      form,
      id,
      type
    }
  });
};

export const refreshState = () => async dispatch => {
  const  db = connectDb();
  const items = await get(db);
  dispatch({
    type: REFRESH_STATE,
    payload: items
  })
};
