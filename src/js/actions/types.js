export const ADD_FORM = 'ADD_FORM';
export const DELETE_FORM = 'DELETE_FORM';
export const GET_FORMS = 'GET_FORMS';
export const UPDATE_FORM = 'UPDATE_FORM';
export const REFRESH_STATE = 'REFRESH_STATE';
