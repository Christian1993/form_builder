import React, {Component} from 'react';

import {connect} from 'react-redux';
import {updateForm} from '../actions/formActions';

class Answer extends Component{
  render(){
    const {id, form} = this.props;
    return(
      <div className="form__group">
        <span className="form__label">Answer</span>
        <input className="form__input" type="text" id={id} value={form.answer} onChange={()=> this.props.updateForm(form, id, 'answer')}/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  forms: state.forms.forms
});

export default connect(mapStateToProps, {updateForm})(Answer);
