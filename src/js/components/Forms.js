import React, {Component} from 'react';
import {connect} from 'react-redux';
import Form from './Form';
import {getForms, addForm, deleteForm, refreshState} from '../actions/formActions';

class Forms extends Component {
  componentDidMount() {
    this.props.getForms();
  }

  componentWillMount() {
    this.props.refreshState();
  }

  render() {
    return (
      <>
      {this.props.forms.map(form => <Form form={form} key={form.id} addForm={this.props.addForm}
                                          deleteForm={this.props.deleteForm}/>)}
      <button className="button button--new-form" onClick={() => this.props.addForm(undefined)}>Add new form</button>
      </>
    );
  }
}

const mapStateToProps = state => ({
  forms: state.forms.forms
});

export default connect(mapStateToProps, {getForms, addForm, deleteForm, refreshState})(Forms);
