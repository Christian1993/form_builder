import React, {Component} from 'react';

import {connect} from 'react-redux';
import {updateForm} from '../actions/formActions';

class AnswerBool extends Component {
  render() {
    const {id, form} = this.props;
    return (
      <div className="form__group">
        <span className="form__label">Answer</span>
        <select className="form__input form__input--select" id={id} onChange={() => this.props.updateForm(form, id, 'answer')}>
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </select>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  forms: state.forms.forms
});

export default connect(mapStateToProps, {updateForm})(AnswerBool);
