import React, {Component} from 'react';

import {connect} from 'react-redux';
import {updateForm} from '../actions/formActions';

class Question extends Component {
  render(){
    const {form, id} = this.props;
    return(
      <div className="form__group">
        <span className="form__label">Question</span>
        <input className="form__input" id={id} value={form.question} type="text" onChange={()=> this.props.updateForm(form, id, 'question')}/>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  forms: state.forms.forms
});

export default connect(mapStateToProps, {updateForm})(Question);