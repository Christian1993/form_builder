import React, {Component} from 'react';

import {connect} from 'react-redux';
import {updateForm} from '../actions/formActions';

class Condition extends Component {
  render() {
    const {id, form} = this.props;

    return (
      <div className="form__group">
        <span className="form__label">Condition</span>
        <select className="form__input form__input--select" value={form.condition} id={id} onChange={()=> this.props.updateForm(form, id, 'condition')}>
          <option value="eq">Equals</option>
          {(form.parent.type === 'bool' || form.parent.type === 'text') ? null :
            <>
              <option value="lt">Less than</option>
              <option value="gt">Greater than</option>
            </>
          }
        </select>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  forms: state.forms.forms
});

export default connect(mapStateToProps, {updateForm})(Condition);
