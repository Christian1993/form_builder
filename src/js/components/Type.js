import React, {Component} from 'react';

import {connect} from 'react-redux';
import {updateForm} from '../actions/formActions';

class Type extends Component {
  render() {
    const {id, form} = this.props;
    return (
      <div className="form__group">
        <span className="form__label">Type</span>
        <select className="form__input form__input--select" value={form.type} id={id} onChange={() => this.props.updateForm(form, id, 'type')}>
          <option value="text">Text</option>
          <option value="number">Number</option>
          <option value="bool">Yes/No</option>
        </select>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  forms: state.forms.forms
});

export default connect(mapStateToProps, {updateForm})(Type);
