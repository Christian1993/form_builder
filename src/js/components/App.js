import React, {Component} from 'react';
import {Provider} from 'react-redux';
import store from '../store';

import Forms from './Forms';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <main className="main">
          <Forms/>
        </main>
      </Provider>
    );
  }
}

export default App;
