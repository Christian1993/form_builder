import React, {Component} from 'react';

import Question from './Question';
import Type from './Type';
import Condition from './Condition';
import Answer from './Answer';
import AnswerBool from './AnswerBool';

class Form extends Component {

  createForm = () => {
    const form = [];
    const {parent, type, id} = this.props.form;
    if (!Array.isArray(parent)) {
      form.push(<Condition id={`condition-${id}`} key={`con-${id}`} form={this.props.form} />);
      if (type === 'bool') {
        form.push(<AnswerBool id={`answer-${id}`} key={`ansB-${id}`} form={this.props.form} />);
      } else {
        form.push(<Answer id={`answer-${id}`} key={`ans-${id}`} form={this.props.form} />)
      }
    }
    return form;
  };

  render() {
    const {form} = this.props;
    return (
      <div className="form" id={form.id}>
        {this.createForm()}
        <Question form={form} id={`question-${form.id}`}/>
        <Type form={form} id={`type-${form.id}`} />

        <button className="button button--add"  onClick={() => this.props.addForm(form)}>Add Sub-Input</button>
        <button className="button button--delete" onClick={() => this.props.deleteForm(form)}>Delete</button>
        {form.children.map(child => <Form form={child} key={child.id} addForm={this.props.addForm} deleteForm={this.props.deleteForm}/>)}
      </div>
    );
  }
}

export default Form;