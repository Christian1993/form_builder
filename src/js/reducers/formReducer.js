import {ADD_FORM, DELETE_FORM, GET_FORMS, UPDATE_FORM, REFRESH_STATE} from '../actions/types';
import {search} from '../helpers/';

import {connectDb, set} from '../helpers'
const db = connectDb();


const initialState = {
  forms: []
};

export default function (state = initialState, action) {
  const [...newForms] = state.forms;

  switch (action.type) {
    case ADD_FORM: {
      const {draft} = action.payload;
      if (draft.parent !== state.forms) {
        const parentElement = search(newForms, draft.parent.id);
        parentElement.children.push(draft);
      } else {
        newForms.push(draft);
      }
      set(db, newForms);
      return {
        ...state,
        forms: newForms
      };
    }

    case GET_FORMS:
      return {...state};

    case DELETE_FORM: {
      let parent = search(newForms, action.payload.parent.id);

      if (Array.isArray(parent)) {
        parent = newForms;
        const indexOfChild = parent.indexOf(action.payload);
        parent.splice(indexOfChild, 1);
      } else {
        const indexOfChild = parent.children.indexOf(action.payload);
        parent.children.splice(indexOfChild, 1);
      }
      set(db, newForms);
      return {
        ...state,
        forms: newForms
      };
    }

    case UPDATE_FORM: {
      const {form, id, type} = action.payload;
      const input = document.querySelector('#' + id);
      const [...newForms] = state.forms;

      let item = search(newForms, form.id);
      if (type === 'question') {
        item.question = input.value;
      } else if (type === 'answer') {
        item.answer = input.value;
      } else if (type === 'condition') {
        item.condition = input.value;
      } else if (type === 'type') {
        item.type = input.options[input.selectedIndex].value;
        if (item.type === 'text' || item.type === 'bool') {
          console.log(item.type);
          item.children.forEach(child => child.condition = 'equals');
        }
      }
      set(db, newForms);
      return {
        ...state,
        forms: newForms
      };
    }
    case REFRESH_STATE: {
      return {
        ...state,
        forms: action.payload || []
      };
    }

    default:
      return {...state};
  }
};
