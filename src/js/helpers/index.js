import idb from 'idb';

export const search = (arr, match) => {
  let result = arr;

  function recurse(arr, match) {
    for (let i = 0, len = arr.length; i < len; i++) {
      if (arr[i].id === match) {
        result = arr[i];
        break;
      } else if (arr[i].children) {
        recurse(arr[i].children, match);
      }
    }
  }

  recurse(arr, match);
  return result;
};

export const connectDb = () => {
  if (!('indexedDB' in window)) {
    alert('Functionality not available, please open other browser...');
    return;
  }
  const dbPromise = idb.open('stateDb', 1, (upgradeDb) => {
    if (!upgradeDb.objectStoreNames.contains('stateDb')) {
      const state = upgradeDb.createObjectStore('state');
      state.createIndex('state', 'state');
    }
  });
  return dbPromise
};

export const get = (dbPromise) => {
  return dbPromise.then(db => {
    const tx = db.transaction('state', 'readonly');
    const store = tx.objectStore('state');
    return store.get(1);
  })
};

export const set = (dbPromise, val) => {
  return dbPromise.then(db => {
    const tx = db.transaction('state', 'readwrite');
    const store = tx.objectStore('state');
    store.put(val, 1);
    return tx.complete;
  })
};
